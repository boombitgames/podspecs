#
# Be sure to run `pod lib lint HelloWorldPod.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |testVariable|
  testVariable.name             = 'HelloWorldPod'
  testVariable.version          = '0.5.38'
  testVariable.summary          = 'A test Pod.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  testVariable.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC

  testVariable.homepage         = 'https://bitbucket.org/boombitgames/helloworldpod/src'
  testVariable.license          = { :type => 'Proprietary software', :file => 'LICENSE' }
  testVariable.author           = { 'CORE' => 'core@boombit.com' }
  testVariable.source           = { :git => 'git@bitbucket.org:boombitgames/helloworldpod.git', :tag => testVariable.version.to_s }

  testVariable.swift_version = '5.0'
  testVariable.ios.deployment_target = '10.0'

  testVariable.source_files = 'HelloWorldPod/Classes/**/*'
  
  testVariable.dependency 'CORCommon', '0.0.9'
  testVariable.frameworks = 'AdSupport', 'Foundation', 'AppTrackingTransparency'
end
