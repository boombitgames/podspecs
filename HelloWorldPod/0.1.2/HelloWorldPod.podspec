#
# Be sure to run `pod lib lint HelloWorldPod.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'HelloWorldPod'
  s.version          = '0.1.2'
  s.summary          = 'A test pod.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC

  s.homepage         = 'https://bitbucket.org/boombitgames/helloworldpod/src'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'Proprietary software', :file => 'LICENSE' }
  s.author           = { 'CORE' => 'core@boombit.com' }
  s.source           = { :git => 'https://bitbucket.org/boombitgames/helloworldpod.git', :tag => s.version.to_s }

  s.ios.deployment_target = '9.0'

  s.source_files = 'HelloWorldPod/Classes/**/*'
  
  # s.resource_bundles = {
  #   'HelloWorldPod' => ['HelloWorldPod/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
  s.frameworks = 'AdSupport', 'Foundation'
end
