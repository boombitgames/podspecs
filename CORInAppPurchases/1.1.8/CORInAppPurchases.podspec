Pod::Spec.new do |s|
  s.name             = 'CORInAppPurchases'
  s.version          = '1.1.8'

  s.summary          = 'Native components for the In-App Purchases Core module'
  s.homepage         = 'https://bitbucket.org/boombitgames/corinapppurchases'
  s.license          = { :type => 'Proprietary software', :file => 'LICENSE' }
  s.authors          = { 'CORE' => 'core@boombit.com' }

  s.source           = { :git => 'https://bitbucket.org/boombitgames/corinapppurchases.git', :tag => s.version.to_s }

  s.swift_version = '5.0'

  s.platform = :ios
  s.ios.deployment_target = '10.0'

  s.source_files = 'CORInAppPurchases/Classes/**/*'
  
  s.frameworks = 'StoreKit'
  
  s.dependency 'CORCommon', '~> 0.0.12'
end
