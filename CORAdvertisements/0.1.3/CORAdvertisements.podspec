#
# Be sure to run `pod lib lint CORAdvertisements.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'CORAdvertisements'
  s.version          = '0.1.3'
  s.summary          = 'Native components for Advertisemens module'
  s.homepage         = 'https://bitbucket.org/boombitgames/coradvertisements/src'
  s.license          = { :type => 'Proprietary software', :file => 'LICENSE' }
  s.author           = { 'CORE' => 'core@boombit.com' }
  s.source           = { :git => 'https://bitbucket.org/boombitgames/coradvertisements.git', :tag => s.version.to_s }
  s.ios.deployment_target = '9.0'
  s.source_files = 'CORAdvertisements/Classes/**/*'

  s.frameworks = 'AdSupport'

  # s.resource_bundles = {
  #   'CORAdvertisements' => ['CORAdvertisements/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end
