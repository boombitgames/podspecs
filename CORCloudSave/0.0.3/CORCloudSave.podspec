#
# Be sure to run `pod lib lint CORCloudSave.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'CORCloudSave'
  s.version          = '0.0.3'
  s.summary          = 'Cloud save pod.'
  s.homepage         = 'https://bitbucket.org/boombitgames/corcloudsave/src'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.authors           = { 'CORE' => 'core@boombit.com' }
  s.source           = { :git => 'https://bitbucket.org/boombitgames/corcloudsave.git', :tag => s.version.to_s }
  s.static_framework = true
  s.swift_versions        = '5.0'

  s.ios.deployment_target = '11.0'

  s.source_files = 'CORCloudSave/Classes/**/*'
  s.dependency 'CORCommon', '~> 0.1.1'
  # s.resource_bundles = {
  #   'CORCloudSave' => ['CORCloudSave/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end
