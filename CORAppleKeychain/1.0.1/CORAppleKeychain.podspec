#
# Be sure to run `pod lib lint CORAppleKeychain.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'CORAppleKeychain'
  s.version          = '1.0.1'
  s.summary          = 'Native components for Apple Keychain module.'
  s.homepage         = 'https://bitbucket.org/boombitgames/corapplekeychain/src'
  s.license          = { :type => 'Proprietary software', :file => 'LICENSE' }
  s.author           = { 'CORE' => 'core@boombit.com' }
  s.source           = { :git => 'git@bitbucket.org:boombitgames/corapplekeychain.git', :tag => s.version.to_s }
  s.ios.deployment_target = '10.0'
  s.source_files = 'CORAppleKeychain/Classes/**/*'
  s.dependency 'CORCommon'
  
  # s.resource_bundles = {
  #   'CORAppleKeychain' => ['CORAppleKeychain/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit'
end
