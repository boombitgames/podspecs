#
# Be sure to run `pod lib lint CORAppLinks.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'CORAppLinks'
  s.version          = '1.1.0'
  s.summary          = 'Native components for App Links module.'
  s.homepage         = 'https://bitbucket.org/boombitgames/corapplinks/src'
  s.license          = { :type => 'Proprietary software', :file => 'LICENSE' }
  s.author           = { 'CORE' => 'core@boombit.com' }
  s.source           = { :git => 'https://bitbucket.org/boombitgames/corapplinks.git', :tag => s.version.to_s }
  s.ios.deployment_target = '10.0'
  s.source_files = 'CORAppLinks/Classes/**/*'
  s.dependency 'CORCommon', '~> 0.1.1'
  
  # s.resource_bundles = {
  #   'CORAppLinks' => ['CORAppLinks/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end
