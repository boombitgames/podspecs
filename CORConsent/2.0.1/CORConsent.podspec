Pod::Spec.new do |s|
  s.name             = 'CORConsent'
  s.version          = '2.0.1'

  s.summary          = 'Native components for the Consent Core module'
  s.homepage         = 'https://bitbucket.org/boombitgames/corconsent'
  s.license          = { :type => 'Proprietary software', :file => 'LICENSE' }
  s.authors          = { 'CORE' => 'core@boombit.com' }

  s.source           = { :git => 'https://bitbucket.org/boombitgames/corconsent.git', :tag => s.version.to_s }

  s.platform = :ios
  s.ios.deployment_target = '10.0'

  s.source_files = 'CORConsent/Classes/**/*'
  
  s.frameworks = [
      "AdSupport",
      "AppTrackingTransparency"
    ]
end
