Pod::Spec.new do |s|
  s.name    = 'CORCommon'
  s.version = '0.0.6'

  s.summary  = 'Components and helpers for other Pods'
  s.homepage = 'https://bitbucket.org/boombitgames/corcommonpod'
  s.license          = { :type => 'Proprietary software', :file => 'LICENSE' }
  s.author           = { 'CORE' => 'core@boombit.com' }
  s.source           = { :git => 'https://bitbucket.org/boombitgames/corcommonpod.git', :tag => s.version.to_s }

  s.swift_version = '5.0'

  s.platform = :ios
  s.ios.deployment_target = '10.0'

  s.source_files = 'CORCommon/Classes/**/*'
end
