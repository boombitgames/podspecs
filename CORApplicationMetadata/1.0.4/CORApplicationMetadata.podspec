#
# Be sure to run `pod lib lint CORApplicationMetadata.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'CORApplicationMetadata'
  s.version          = '1.0.4'
  s.summary          = 'Native components for Application Metadata module.'
  s.homepage         = 'https://bitbucket.org/boombitgames/corapplicationmetadata/src'
  s.license          = { :type => 'Proprietary software', :file => 'LICENSE' }
  s.author           = { 'CORE' => 'core@boombit.com' }
  s.source           = { :git => 'https://bitbucket.org/boombitgames/corapplicationmetadata.git', :tag => s.version.to_s }
  s.ios.deployment_target = '10.0'

  s.source_files = 'CORApplicationMetadata/Classes/**/*'
  
  # s.resource_bundles = {
  #   'CORApplicationMetadata' => ['CORApplicationMetadata/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end
