#
# Be sure to run `pod lib lint CORFirebaseSDK.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'CORFirebaseSDK'
  s.version          = '1.0.33'
  s.summary          = 'Native components for FirebaseSDK module.'
  s.homepage         = 'https://bitbucket.org/boombitgames/corfirebasesdk/src'
  s.license          = { :type => 'Proprietary software', :file => 'LICENSE' }
  s.author           = { 'CORE' => 'core@boombit.com' }
  s.source           = { :git => 'git@bitbucket.org:boombitgames/corfirebasesdk.git', :tag => s.version.to_s }
  s.ios.deployment_target = '11.0'
  s.static_framework = true
  s.source_files = 'CORFirebaseSDK/Classes/**/*'
  s.swift_versions = '5.0'
  # s.resource_bundles = {
  #   'CORFirebaseSDK' => ['CORFirebaseSDK/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'

  s.dependency 'Firebase/Analytics', '10.25.0'
  s.dependency 'Firebase/Auth', '10.25.0'

end
