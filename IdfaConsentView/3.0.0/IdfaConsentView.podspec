#
# Be sure to run `pod lib lint IdfaConsentView.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'IdfaConsentView'
  s.version          = '3.0.0'
  s.summary          = 'IDFA consent request screen using a default system popup'

  s.description      = <<-DESC
  Provides a default implementation of an Advertising Identifier consent request screen using a default system popup.
  This Consent View is required only on iOS.
                       DESC

  s.homepage         = 'https://bitbucket.org/boombitgames/idfaconsentviewpod'
  s.license          = { :type => 'Proprietary software', :file => 'LICENSE' }
  s.author           = { 'CORE' => 'core@boombit.com' }
  s.source           = { :git => 'https://bitbucket.org/boombitgames/idfaconsentviewpod.git', :tag => s.version.to_s }

  s.ios.deployment_target = '9.0'

  s.source_files = 'IdfaConsentView/Classes/**/*'
  
  s.frameworks = 'AdSupport', 'Foundation', "AppTrackingTransparency"
end
