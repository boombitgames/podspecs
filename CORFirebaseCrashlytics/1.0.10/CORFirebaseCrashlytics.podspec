#
# Be sure to run `pod lib lint CORFirebaseCrashlytics.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'CORFirebaseCrashlytics'
  s.version          = '1.0.10'
  s.summary          = 'Native components for Firebase Crashlytics module.'
  s.homepage         = 'https://bitbucket.org/boombitgames/corfirebasecrashlytics/src'
  s.license          = { :type => 'Proprietary software', :file => 'LICENSE' }
  s.author           = { 'CORE' => 'core@boombit.com' }
  s.source           = { :git => 'git@bitbucket.org:boombitgames/corfirebasecrashlytics.git', :tag => s.version.to_s }
  s.ios.deployment_target = '11.0'

  s.source_files = 'CORFirebaseCrashlytics/Classes/**/*'
  
  # s.resource_bundles = {
  #   'CORFirebaseCrashlytics' => ['CORFirebaseCrashlytics/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'

  s.dependency 'Firebase/Analytics', '10.24.0'
  s.dependency 'Firebase/Crashlytics', '10.24.0'
end
