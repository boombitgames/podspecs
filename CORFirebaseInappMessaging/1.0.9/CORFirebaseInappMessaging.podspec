#
# Be sure to run `pod lib lint CORFirebaseInappMessaging.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'CORFirebaseInappMessaging'
  s.version          = '1.0.9'
  s.summary          = 'Native components for Firebase Inapp Messaging module.'
  s.homepage         = 'https://bitbucket.org/boombitgames/corfirebaseinappmessaging/src'
  s.license          = { :type => 'Proprietary software', :file => 'LICENSE' }
  s.author           = { 'CORE' => 'core@boombit.com' }
  s.source           = { :git => 'git@bitbucket.org:boombitgames/corfirebaseinappmessaging.git', :tag => s.version.to_s }
  s.ios.deployment_target = '11.0'
  s.static_framework = true
  s.source_files = 'CORFirebaseInappMessaging/Classes/**/*'
  
  # s.resource_bundles = {
  #   'CORFirebaseInappMessaging' => ['CORFirebaseInappMessaging/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  s.dependency 'Firebase/Analytics', '10.22.0'
  s.dependency 'Firebase/InAppMessaging', '10.22.0'
  s.dependency 'CORCommon'

end
