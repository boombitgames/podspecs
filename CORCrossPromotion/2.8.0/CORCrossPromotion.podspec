
Pod::Spec.new do |s|
  s.name             = 'CORCrossPromotion'
  s.version          = '2.8.0'
  s.summary          = 'A plugin that handles ads tracking, displaying, and presenting products in Appstore supplied by Boombit Cross Promotion network.'

  s.homepage         = 'https://bitbucket.org/boombitgames/corcrosspromotion/src'
  s.license          = { :type => 'Proprietary software', :file => 'LICENSE' }
  s.author           = { 'CORE' => 'core@boombit.com' }
  s.source           = { :git => 'git@bitbucket.org:boombitgames/corcrosspromotion.git', :tag => s.version.to_s }

  s.swift_version = '5.3'
  s.ios.deployment_target = '11.0'

  s.dependency 'CORCommon'
  
  s.frameworks = 'AudioToolbox'

  s.source_files = 'CORCrossPromotion/Classes/**/*'
  
  
  s.resource_bundles = {
    'CORCrossPromotion' => ['CORCrossPromotion/Assets/cormute.caf']
  }
end
