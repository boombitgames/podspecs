
Pod::Spec.new do |s|
  s.name             = 'CORHaptics'
  s.version          = '0.3.0'
  s.summary          = 'A plugin that bridge iOS Haptics API to Unity.'

  s.homepage         = 'https://bitbucket.org/boombitgames/corhaptics/src'
  s.license          = { :type => 'Proprietary software', :file => 'LICENSE' }
  s.author           = { 'CORE' => 'core@boombit.com' }
  s.source           = { :git => 'git@bitbucket.org:boombitgames/corhaptics.git', :tag => s.version.to_s }

  s.swift_version = '5.3'
  s.ios.deployment_target = '13.0'

  s.source_files = 'CORHaptics/Classes/**/*'

  s.frameworks = 'CoreHaptics'
end
