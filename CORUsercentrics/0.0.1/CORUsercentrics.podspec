Pod::Spec.new do |s|
  s.name             = 'CORUsercentrics'
  s.version          = '0.0.1'

  s.summary          = 'Provides access to Usercentrics consent management platform'
  s.homepage         = 'https://bitbucket.org/boombitgames/corusercentrics'
  s.license          = { :type => 'Proprietary software', :file => 'LICENSE' }
  s.authors          = { 'CORE' => 'core@boombit.com' }

  s.source           = { :git => 'https://bitbucket.org/boombitgames/corusercentrics.git', :tag => s.version.to_s }

  s.swift_version = '5.3'

  s.platform = :ios
  s.ios.deployment_target = '12.0'
  
  s.static_framework = true

  s.source_files = 'CORUsercentrics/Classes/**/*'
  
  s.dependency 'CORCommon', '~> 0.1.1'
  s.dependency 'UsercentricsUI', '2.10.2-unity-static'
end
