Pod::Spec.new do |s|
  s.name             = 'CORSharePanel'
  s.version          = '0.0.5'
  s.summary          = 'Enables sharing content.'
  s.license                  = { :type => 'Proprietary software', :file => 'LICENSE' }
  s.homepage                 = 'https://bitbucket.org/boombitgames/corsharepanel'
  s.author                   = { 'CORE' => 'core@boombit.com' }
  s.source           = { :git => 'git@bitbucket.org:boombitgames/corsharepanel.git', :tag => s.version.to_s }
  s.source_files = 'CORSharePanel/Classes/**/*'
  s.platform                 = :ios
  s.ios.deployment_target    = '13.0'
  s.static_framework         = true
  s.swift_version            = '5.0'
end
