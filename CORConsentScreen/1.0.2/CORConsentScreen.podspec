Pod::Spec.new do |s|
  s.name             = 'CORConsentScreen'
  s.version          = '1.0.2'

  s.summary          = 'Native components for the Consent Screen Core module'
  s.homepage         = 'https://bitbucket.org/boombitgames/corconsentscreen'
  s.license          = { :type => 'Proprietary software', :file => 'LICENSE' }
  s.authors          = { 'CORE' => 'core@boombit.com' }

  s.source           = { :git => 'https://bitbucket.org/boombitgames/corconsentscreen.git', :tag => s.version.to_s }

  s.swift_version = '5.0'

  s.platform = :ios
  s.ios.deployment_target = '11.0'

  s.source_files = 'CORConsentScreen/Classes/**/*'
  s.resource_bundle = {
      'CORConsentScreen' => ['CORConsentScreen/Assets/CORConsentScreen.bundle/arrowshape.turn.up.right@2x.png']
  }
  
  s.dependency 'CORCommon', '~> 0.1.1'
end
