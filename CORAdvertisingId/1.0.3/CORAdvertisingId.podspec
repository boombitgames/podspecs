Pod::Spec.new do |s|
  s.name             = 'CORAdvertisingId'
  s.version          = '1.0.3'

  s.summary          = 'Provides access to Advertising ID through relevant native APIs.'
  s.homepage         = 'https://bitbucket.org/boombitgames/coradvertisingid'
  s.license          = { :type => 'Proprietary software', :file => 'LICENSE' }
  s.authors          = { 'CORE' => 'core@boombit.com' }

  s.source           = { :git => 'https://bitbucket.org/boombitgames/coradvertisingid.git', :tag => s.version.to_s }

  s.swift_version = '5.0'

  s.platform = :ios
  s.ios.deployment_target = '10.0'

  s.source_files = 'CORAdvertisingId/Classes/**/*'
  
  s.frameworks = [
    'AdSupport',
    'AppTrackingTransparency'
  ]
  
  s.dependency 'CORCommon', '~> 0.1.1'
end
