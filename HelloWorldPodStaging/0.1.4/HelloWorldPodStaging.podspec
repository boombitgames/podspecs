#
# Be sure to run `pod lib lint HelloWorldPodStaging.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'HelloWorldPodStaging'
  s.version          = '0.1.4'
  s.summary          = 'A test Pod.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC

  s.homepage         = 'https://bitbucket.org/boombitgames/helloworldpodstaging/src'
  s.license          = { :type => 'Proprietary software', :file => 'LICENSE' }
  s.author           = { 'CORE' => 'core@boombit.com' }
  s.source           = { :git => 'git@bitbucket.org:boombitgames/helloworldpodstaging.git', :tag => s.version.to_s }

  s.swift_version = '5.0'
  s.ios.deployment_target = '10.0'

  s.source_files = 'HelloWorldPodStaging/Classes/**/*'

  s.dependency 'CORCommon', '0.0.9'
  s.frameworks = 'AdSupport', 'Foundation', 'AppTrackingTransparency'
end
