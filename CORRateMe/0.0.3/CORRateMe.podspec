Pod::Spec.new do |s|
  s.name                     = 'CORRateMe'
  s.version                  = '0.0.3'
  s.summary                  = 'Enables requesting in-app review.'
  s.license                  = { :type => 'Proprietary software', :file => 'LICENSE' }
  s.homepage                 = 'https://bitbucket.org/boombitgames/corrateme'
  s.author                   = { 'CORE' => 'core@boombit.com' }

  s.source                   = { :git => 'git@bitbucket.org:boombitgames/corrateme.git', :tag => s.version.to_s }
  s.source_files              = 'CORRateMe/Classes/**/*'
  s.platform                 = :ios
  s.ios.deployment_target    = '12.0'
  s.static_framework         = true
  s.swift_version            = '5.0'
end