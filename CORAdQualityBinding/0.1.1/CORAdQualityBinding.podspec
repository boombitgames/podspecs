#
# Be sure to run `pod lib lint CORAdQualityBinding.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'CORAdQualityBinding'
  s.version          = '0.1.1'
  s.summary          = 'Unity binding for Level Play AdQuality.'
  s.homepage         = 'https://bitbucket.org/boombitgames/coradqualitybinding/src'
  s.license          = { :type => 'Proprietary software', :file => 'LICENSE' }
  s.author           = { 'CORE' => 'core@boombit.com' }
  s.source           = { :git => 'git@bitbucket.org:boombitgames/coradqualitybinding.git', :tag => s.version.to_s }
  s.ios.deployment_target = '12.0'
  s.source_files = 'CORAdQualityBinding/Classes/**/*'
  s.dependency 'CORCommon'
  s.dependency 'IronSourceAdQualitySDK', '~> 7.20.1'
end
