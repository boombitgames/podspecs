#
# Be sure to run `pod lib lint CORFirebaseCloudMessaging.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'CORFirebaseCloudMessaging'
  s.version          = '1.0.7'
  s.summary          = 'Native components for Firebase Cloud Messaging module.'
  s.homepage         = 'https://bitbucket.org/boombitgames/corfirebasecloudmessaging/src'
  s.license          = { :type => 'Proprietary software', :file => 'LICENSE' }
  s.author           = { 'CORE' => 'core@boombit.com' }
  s.source           = { :git => 'https://bitbucket.org/boombitgames/corfirebasecloudmessaging.git', :tag => s.version.to_s }
  s.ios.deployment_target = '10.0'
  s.static_framework = true
  s.source_files = 'CORFirebaseCloudMessaging/Classes/**/*'
  
  # s.resource_bundles = {
  #   'CORFirebaseCloudMessaging' => ['CORFirebaseCloudMessaging/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'

  s.dependency 'Firebase/Analytics', '8.7.0'
  s.dependency 'Firebase/Auth', '8.7.0'
  s.dependency 'Firebase/Messaging', '8.7.0'
  s.dependency 'CORCommon', '~> 0.1.1'


end
